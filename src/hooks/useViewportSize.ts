import { useEffect, useState } from 'react'

interface ISizes {
  width: number
  height: number
}

function getSizes(): ISizes {
  return { width: window.innerWidth, height: window.innerHeight }
}

export function useViewportSize(): ISizes {
  const [sizes, setSizes] = useState<ISizes>(() => getSizes())

  function applySizes(): void {
    setSizes(getSizes())
  }
  useEffect(() => {
    window.addEventListener('resize', applySizes)

    return () => window.removeEventListener('resize', applySizes)
  }, [])

  return sizes
}
