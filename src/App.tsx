import { useViewportSize } from '@mantine/hooks/useViewportSize'

export function App() {
  const { height, width } = useViewportSize()

  return (
    <>
      <div className="flex justify-center p-2 bg-blue-200 text-2xl max-w-64">
        Width: {width}, height: {height}
      </div>
    </>
  )
}
